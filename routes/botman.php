<?php

use App\Http\Controllers\BotManController;

$botman = resolve('botman');


// ---- Sample Original
// $botman->hears('Hi', function ($bot) {
//     $bot->reply('Hello!');
// });
// $botman->hears('Start conversation', BotManController::class . '@startConversation');


// ---- Sample main page


// $botman->hears('Hello BotMan!', function ($bot) {
//     $bot->reply('Hello!');
//     $bot->ask('Whats your name?', function ($answer, $bot) {
//         $bot->say('Welcome ' . $answer->getText());
//     });
// });

// $botman->listen();



// ---- Sample Spanish


$botman->hears('Hello bot', 'App\Http\Controllers\ChatController@index');

$botman->fallback('App\Http\Controllers\FailChatController@index');
