<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FailChatController extends Controller
{
    function index($bot)
    {
        $bot->reply('Sorry, I did not understand these commands. Here is a list of commands I understand: ...');
    }
}
