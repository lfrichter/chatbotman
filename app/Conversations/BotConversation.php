<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class BotConversation extends Conversation
{
    /**
     * First question
     */
    public function hello()
    {
        $question = Question::create("Olá! Escolha uma opção") //Saludamos al usuario
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Quem é?')->value('who'), //Primera opcion, esta tendra el value who
                Button::create('O que você pode me contar?')->value('info'), //Segunda opcion, esta tendra el value info
            ]);

        // Quando o usuário escolhe a resposta, o valor será enviado aqui:
        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'who') { // Se for o valor quem, ele responderá com esta mensagem
                    $this->say('Sou um chatbot, ajudo você a navegar neste aplicativo,
                    você apenas tem que escrever "Hello bot"');
                    // Se for a informação do valor, chamaremos as opções de função
                } else if ($answer->getValue() === 'info') {
                    $this->options();
                }
            }
        });
    }

    public function options()
    {
        $question = Question::create("O que você quer saber?") //le preguntamos al usuario que quiere saber
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Que horas são?')->value('hour'), //Opción de hora, con value hour
                Button::create('Que dia é hoje?')->value('day'), //Opción de fecha, con value day
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'hour') { // Mostra ao usuário a hora se o valor for hora
                    $hour = date('H:i');
                    $this->say('Agora é ' . $hour);
                } else if ($answer->getValue() === 'day') { // Mostra ao usuário a hora se o valor é data
                    $today = date("d/m/Y");
                    $this->say('Hoje é: ' . $today);
                }
            }
        });
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->hello();
    }
}
